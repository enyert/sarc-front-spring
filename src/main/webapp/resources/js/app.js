'use strict';

// Declare app level module which depends on views, and components
var sarcFront = angular.module('sarcFront', [ 'ngResource', 'ngRoute', 'sarcControllers']);

sarcFront.config(['$routeProvider',
        function($routeProvider){ 
            $routeProvider. 
                when('/', {
                    templateUrl: 'resources/partials/inbox.html',
                    controller: 'TestCtrl'
                }).
                when('/inbox', {
                    templateUrl: 'resources/partials/inbox.html',
                    controller: 'TestCtrl'
                }).
                when('/input', {
                  templateUrl: 'resources/partials/ingresar_datos.html',
                  controller: 'TestCtrl'
                }).
                when('/cargar', {
                    templateUrl: 'resources/partials/cargar_recaudos.html',
                    controller: 'TestCtrl'
                }).
                when('/created', {
                    templateUrl: 'resources/partials/nota_marginal_creada.html',
                    controller: 'TestCtrl'
                }).
                when('/select', {
                    templateUrl: 'resources/partials/seleccionar_tipo_acta.html',
                    controller: 'SeleccionaTipoNotaMarginal'
                }).
                when('/cancel', {
                    templateUrl: 'resources/partials/solicitud_cancelada.html',
                    controller: 'TestCtrl'
                }).
                when('/preview', {
                    templateUrl: 'resources/partials/vista_previa.html',
                    controller: 'TestCtrl'
                })
            //locationProvider.html5Mode(true);
        }]);



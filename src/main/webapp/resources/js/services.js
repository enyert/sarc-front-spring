'use strict';

var sarcServices = angular.module('sarcServices', []);

sarcServices.factory('TipoNotaMarginal', ['$resource', function($resource) {

  return $resource('http://localhost:8080/tipoNotaMarginal');
  
}]);

// sarcServices.provider('mainConfig', function(){
  
// });

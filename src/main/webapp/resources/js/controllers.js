'use strict';

/* Controllers */

var sarcControllers = angular.module('sarcControllers', ['sarcServices']);


sarcControllers.controller('TestCtrl', ['$scope', '$routeParams', 
  function($scope, $routeParams) {
    $scope.hola = "hola";
  }]);

sarcControllers.controller('SeleccionaTipoNotaMarginal', ['$scope', 'TipoNotaMarginal', function($scope, TipoNotaMarginal){
  
    var stnm = this;
  
    $scope.tiposNotaMarginal = TipoNotaMarginal.query(function(){
	return _.map(
	    $scope.tiposNotaMarginal,
	    function(tnm) {
		tnm.datos = JSON.parse(tnm.datos);
		tnm.recaudos = JSON.parse(tnm.recaudos);
	    }
	);
    });
  
}]);
